// Escreva um programa que le um inteiro n.
// Se n for par, escreva 0.
// Se n for impar, escreva 1.

// Casos-teste
// Entrada   Saida
// 0         0
// 5         1
// 12        0
// -7        1
// -8        0