#include <stdio.h>
#include <time.h>   // Para usar a funcao time().
#include <stdlib.h> // Para usar srand() e rand().

int main() {
	// Inicializa gerador de numeros aleatorios.
	srand(time(NULL));
	
	printf("Quantos numeros aleatorios?\n");
	int n; scanf("%d", &n);
	printf("Qual o maior numero a gerar?\n");
	int m; scanf("%d", &m);
	
	// Para c de 0 ate n,
	int c = 0;
	while(c != n) {
		// Gera um numero no intervalo [0, m[ e escreve na tela.
		// (cada chamada a rand() da um valor novo)
		printf("%d ", rand() % m);
		c++;
	}
	printf("\n");
	
	return 0;
}