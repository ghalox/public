// Leia dois horarios no formato horas:minutos:segundos
// e escreva quanto tempo se passou entre o primeiro
// horario e o segundo horario, tambem nesse formato.

// Assuma que os horarios vieram de um relogio digital,
// que vai de 00:00:00 ate 23:59:59. Assuma tambem que
// a diferenca entre os horarios e' menor que um dia.

// Dica: Voce pode usar o scanf pra descrever como e'
// o formato da entrada! Entao se voce usar no codigo
//
//     scanf("%d:%d:%d", &x, &y, &z)
//
// e escrever 01:23:45 quando executar, o valor lido
// para x sera 1, para y sera 23, e para z sera 45.

// Casos-teste
// Entrada             Saida
// 00:00:00 12:32:43   12:32:43
// 12:30:00 22:32:45   10:02:45
// 22:32:45 12:30:00   13:57:15
// 12:00:00 12:00:00   00:00:00