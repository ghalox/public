// Leia um inteiro n, mais n inteiros.
// Leia outro inteiro m. Voce devera imprimir m linhas, da seguinte maneira:
// Na i-esima linha, escreva cada um dos n inteiros elevados a i.

// Exemplo
// -------
// Entrada:
// 4
// -1 2 3 10
// 3
//
// Saida:
// -1 2 3 10
// 1 4 9 100
// -1 8 27 1000