#include <stdio.h>

#define MAX 100

int main(){	
	printf("Voce vai escrever quantos inteiros?\n");
	int n; scanf("%d", &n);
	
	int i, nums[MAX];	
	printf("Escreva %d inteiros, entao.\n", n);
	i = 0;
	while(i < n) {
		scanf("%d", &(nums[i]));
		i = i + 1;
	}
	
	// Escreve os n numeros lidos.
	printf("Numeros lidos: ");
	i = 0;
	while(i < n) {
		printf("%d ", nums[i]);
		i = i + 1;
	}
	printf("\n");
	
	// Escreve o proximo de cada numero.
	printf("Numeros + 1:   ");
	for(i = 0; i < n; i = i + 1){
		printf("%d ", nums[i] + 1);
	}
	printf("\n");
	
	return 0;
}