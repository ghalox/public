// Escreva um programa que recebe tres inteiros x, y e z
// e entao responde quantos ladrilhos quadrados de lado x
// cabem inteiros num retangulo de lados y e z.

// Assuma que os lados dos ladrilhos estarao alinhados
// com os lados do retangulo (ou seja, os ladrilhos nao
// vao estar, por exemplo, na diagonal).

// Alguns casos-teste, pra ver se o programa esta' ok:
//  x  y  z  resposta
//  1  2  3     6
//  3  5  3     1
//  2  4  5     4