// Leia uma quantidade inteira de centavos
// e escreva essa quantidade em reais.

// Dica: o seguinte codigo
//     printf("Numero: %04d", 23);
// escreve
//     Numero: 0023
// na tela, com os zeros na esquerda.

// Casos-teste
//  entrada    saida
//        1     0.01
//      100     1.00
//      537     5.37
//   123456  1234.56