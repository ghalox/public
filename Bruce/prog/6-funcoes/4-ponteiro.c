#include <stdio.h>

void func_um(int a){
	printf("valor recebido: %d\n", a);
	a = 2;
	printf("valor alterado: %d\n", a);
	return;
}

void func_dois(int *a){
	printf("endereco recebido: %d\n", a);
	printf("valor no endereco: %d\n", *a);
	*a = 2;
	printf("valor alterado: %d\n", *a);
	return;
}

int main(){
	int a = 1;
	printf("valor inicial: a = %d\n", a);
	
	func_um(a);
	printf("depois de func_um: a = %d\n", a);
	
	func_dois(&a);
	printf("depois de func_dois: a = %d\n", a);
	
	return 0;
}