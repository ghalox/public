// Leia um inteiro n, seguido de n inteiros.
// Calcule a mediana desses n inteiros, isto e',
// considere os n inteiros em ordem crescente e,
// se n for impar, escreva o valor do meio;
// se n for par, escreva a media aritmetica dos
// dois valores centrais.

// Exemplo
// -------
// Entrada:
// 5
// 5 3 4 2 1
//
// Saida:
// 3

// Entrada:
// 6
// 2 3 5 6 7 9
//
// Saida:
// 10.5