#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>

// Funcoes auxiliares.
void le_senha(char*);
const char *codifica(char*);
const char *decodifica(char*);
int verifica_credenciais(char*, char*);
int usuario_existe(char*);
void adiciona_usuario(char*, char*);
void remove_usuario(char*);

// Funcoes de entrada-saida.
void ativa_escrita(){
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE); 
	DWORD mode = 0;
	GetConsoleMode(hStdin, &mode);
	SetConsoleMode(hStdin, mode | ENABLE_ECHO_INPUT);
}
void desativa_escrita(){
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE); 
	DWORD mode = 0;
	GetConsoleMode(hStdin, &mode);
	SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));
}

// Corpo principal do programa.
int main(){	
	enum estados {login, oi, tchau, menu, lista, novo, exclui, altera, teste, decod, fim};
	enum estados estado = login;
	char usuario[128]; int opc, boss = 0;
	
	do{
		if(estado == login){
			// Le usuario.
			printf("Usuario: ");
			if(scanf("%127s", usuario) == EOF) return 0;
			fflush(stdin);
			
			// Le senha.
			char sen[128];
			printf("Senha: ");
			le_senha(sen);
			
			// Verifica se ok
			if(strcmp(usuario, "boss") == 0){
				if(strcmp(sen, decodifica("desafio")) == 0){
					boss = 1;
					estado = oi;
				}else
					printf("\nLogin incorreto.\n");
			}else if(verifica_credenciais(usuario, sen))
				estado = oi;
			else
				printf("\nLogin incorreto.\n");
				
		}else if(estado == oi){
			if(boss) printf("\nHey, chefe! :D\n");
			else printf("\nOla, %s!\n", usuario);
			estado = menu;
			
		}else if(estado == tchau){
			printf("\nTchau, %s!\n", usuario);
			boss = 0;
			estado = login;
			
		}else if(estado == menu){
			printf("\n===== Menu Principal =====\n");
			printf("Selecione uma opcao:\n");
			printf("1. Listar usuarios\n");
			printf("2. Adicionar usuario\n");
			printf("3. Remover usuario\n");
			printf("4. Alterar senha\n");
			printf("5. Deslogar\n");
			printf("0. Encerrar sistema\n");
			printf(">");
			scanf("%d", &opc);
			
			if(opc == 1) estado = lista;
			else if(opc == 2) estado = novo;
			else if(opc == 3) estado = exclui;
			else if(opc == 4) estado = altera;
			else if(opc == 5) estado = tchau;
			else if(opc == 0){
				printf("Deseja mesmo encerrar (1=sim, 0=nao)? ");
				scanf("%d", &opc);
				if(opc == 1) estado = fim;
			}
			else if(opc == 32) estado = teste;
			else if(opc == 23) estado = decod;
			else printf("Opcao invalida!\n");
			
		}else if(estado == lista){
			// Abre banco de dados de usuarios.
			FILE *usuarios = fopen("usuarios", "rb");
			if(usuarios == NULL){
				fprintf(stderr, "Erro: nao foi possivel abrir banco de dados de usuarios!!!");
				exit(-1);
			}
			
			// Lista usuarios.
			char t_login[128];
			printf("boss\n"); // boss sempre existe.
			while(fscanf(usuarios, "%s %*s", t_login) != EOF)
				printf("%s\n", t_login);
			
			// Fecha banco de dados.
			fclose(usuarios);
			
			estado = menu;
			
		}else if(estado == novo){
			if(!boss){
				printf("Credenciais insuficientes para adicionar usuario. ):\n");
				estado = menu;
				continue;
			}
			
			printf("Novo usuario: ");
			
			char novo_login[128], t_login[128];
			if(scanf("%127s", novo_login) == EOF){
				estado = menu;
				continue;
			}
			fflush(stdin);
			
			// Confere se usuario ja existe.
			if(usuario_existe(novo_login)){
				printf("Usuario ja existe!\n");
				continue;
			}
		
			// Recebe uma senha.
			char senha[128];
			printf("Senha: ");
			le_senha(senha);
			
			// Inclui usuario-senha no fim do banco de dados.
			adiciona_usuario(novo_login, senha);
			
			printf("\nUsuario %s criado com sucesso!\n", novo_login);
			estado = menu;
			
		}else if(estado == exclui){
			if(!boss){
				printf("Credenciais insuficientes para remover usuario. ):\n");
				estado = menu;
				continue;
			}
			
			printf("Usuario para remover: ");
			
			char rem_login[128], t_login[128];
			if(scanf("%127s", rem_login) == EOF){
				estado = menu;
				continue;
			}
			fflush(stdin);
			
			// Confere se usuario existe.
			if(!usuario_existe(rem_login)){
				printf("Usuario nao existe!\n");
				estado = menu;
				continue;
			}
			
			// Remove usuario do banco de dados.
			remove_usuario(rem_login);
			
			printf("\nUsuario %s removido com sucesso!\n", rem_login);
			estado = menu;
			
		}else if(estado == altera){
			if(boss){
				printf("Nao faca isso, chefe! ):\n");
				estado = menu;
				continue;
			}
			
			// Verifica senha.
			char sen[128];
			printf("Senha: ");
			le_senha(sen);
			
			if(!verifica_credenciais(usuario, sen)){
				printf("\nSenha incorreta.\n");
				estado = menu;
				continue;
			}
			
			// Recebe nova senha.
			printf("\nNova senha: ");
			le_senha(sen);
			
			// Altera senha no banco de dados.
			remove_usuario(usuario);
			adiciona_usuario(usuario, sen);
			
			printf("\nSenha alterada com sucesso!\n");
			estado = menu;
			
		}else if(estado == teste){
			char senha_teste[128];
			printf("Senha de teste: ");
			if(scanf("%127s", senha_teste) == EOF){
				estado = menu;
				continue;
			}
			fflush(stdin);
			
			const char *chave = codifica(senha_teste);
			printf("Chave: %s\n", chave);
			
		}else if(estado == decod){
			char chave[128];
			if(scanf("%127s", chave) == EOF){
				estado = menu;
				continue;
			}
			fflush(stdin);
			printf("%s\n", decodifica(chave));
		}else{
			estado = fim;
		}
	} while( estado != fim );
	
	return 0;
}

// Desativa escrita para tela, le algo do teclado, reativa de novo.
// Garantimos que lemos no maximo 127 caracteres; e descartamos todo o restante do buffer.
void le_senha(char *senha){
	desativa_escrita();
	scanf("%127s", senha);
	fflush(stdin);
	ativa_escrita();
	return;
}

// Recebe uma senha e devolve versao codificada.
const char *codifica(char *senha){
	int i;
	char segredo[] = "segredo";
	static char chave[128];
	
	// '!' eh primeiro imprimivel. '~' eh ultimo imprimivel.
	for(i = 0; senha[i] != '\0' && i < 127; i++)
		chave[i] = (segredo[i % 7] - '!' + senha[i] - '!') % ('~' - '!' + 1) + '!';
	chave[i] = '\0';
	
	return chave;
}

const char *decodifica(char *chave){
	int i;
	char segredo[] = "segredo";
	static char senha[128];
	
	// '!' eh primeiro imprimivel. '~' eh ultimo imprimivel.
	for(i = 0; chave[i] != '\0' && i < 127; i++)
		senha[i] = (chave[i] - segredo[i % 7] + ('~' - '!' + 1)) % ('~' - '!' + 1) + '!';
	senha[i] = '\0';
	
	return senha;
}

// Devolve 1 se usuario-senha constam no banco de dados;
// ou 0, caso contrario.
int verifica_credenciais(char *usuario, char *senha){
	// Abre banco de dados de usuarios.
	FILE *usuarios = fopen("usuarios", "r+b");
	if(usuarios == NULL){
		fprintf(stderr, "Erro: nao foi possivel abrir banco de dados de usuarios!!!");
		exit(-1);
	}
	
	// Busca usuario no banco de dados.
	fseek(usuarios, 0, SEEK_SET); // Vai pro comeco do arquivo.
	char t_login[128], t_chave[128];
	while(fscanf(usuarios, "%s %s", t_login, t_chave) != EOF && strcmp(t_login, usuario) != 0); // Compara linha um a um ate achar usuario.
	
	// Fecha banco de dados.
	fclose(usuarios);
	
	const char *chave = codifica(senha);
	return (strcmp(t_login, usuario) == 0) && (strcmp(t_chave, chave) == 0);
}

// Devolve 1 se usuario existe; ou 0, caso contrario.
int usuario_existe(char *usuario){
	// Abre banco de dados de usuarios.
	FILE *usuarios = fopen("usuarios", "r+b");
	if(usuarios == NULL){
		fprintf(stderr, "Erro: nao foi possivel abrir banco de dados de usuarios!!!");
		exit(-1);
	}
	
	// Busca usuario no banco de dados.
	char t_login[128];
	fseek(usuarios, 0, SEEK_SET);
	while(fscanf(usuarios, "%s %*s", t_login) != EOF && strcmp(usuario, t_login) != 0);
	
	// Fecha banco de dados.
	fclose(usuarios);
	
	return strcmp(usuario, t_login) == 0;
}

void adiciona_usuario(char *usuario, char *senha){
	// Abre banco de dados de usuarios.
	FILE *usuarios = fopen("usuarios", "r+b");
	if(usuarios == NULL){
		fprintf(stderr, "Erro: nao foi possivel abrir banco de dados de usuarios!!!");
		exit(-1);
	}
	
	// Adiciona no fim do banco de dados.
	const char *chave = codifica(senha);
	fseek(usuarios, 0, SEEK_END);
	fprintf(usuarios, "%s\t%s\n", usuario, chave);
	
	// Fecha banco de dados.
	fclose(usuarios);
	
	return;
}

void remove_usuario(char *usuario){
	// Abre banco de dados de usuarios.
	FILE *usuarios = fopen("usuarios", "r+b");
	if(usuarios == NULL){
		fprintf(stderr, "Erro: nao foi possivel abrir banco de dados de usuarios!!!");
		exit(-1);
	}
	
	// Busca usuario no banco de dados.
	char t_login[128]; int pos;
	fseek(usuarios, 0, SEEK_SET);
	while(fscanf(usuarios, "%s %*s", t_login) != EOF && strcmp(usuario, t_login) != 0) pos = ftell(usuarios);
	
	// Move todos os usuarios posteriores uma linha para cima.
	char bloco[260];
	int tam_bloco = ftell(usuarios) - pos, lido;
	fseek(usuarios, 1, SEEK_CUR);
	while((lido = fread(bloco, sizeof(char), tam_bloco, usuarios)) == tam_bloco){
		fseek(usuarios, -2*tam_bloco, SEEK_CUR);
		fwrite(bloco, sizeof(char), tam_bloco, usuarios);
		fseek(usuarios, tam_bloco, SEEK_CUR);
	}
	fseek(usuarios, -tam_bloco-lido, SEEK_CUR);
	fwrite(bloco, sizeof(char), lido, usuarios);
	pos = ftell(usuarios);
	fclose(usuarios);
	
	// Diminui tamanho do arquivo do banco de dados.
	int fh = _open("usuarios", _O_RDWR | _O_BINARY);
	_chsize(fh, pos);
	_close(fh);
	
	return;
}