// Leia um valor inteiro n, que é a duração
// de um evento em segundos, e escreva esse
// tempo no formato horas:minutos:segundos.

// Casos-teste
//      n   resposta
//      1      0:0:1
//     60      0:1:0
//     85     0:1:25
//    556     0:9:16
//   3630     1:0:30
//  14444     4:0:44
// 140153   38:55:53