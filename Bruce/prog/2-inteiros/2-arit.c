#include <stdio.h>

int main()
{
	int numero_um, numero_dois;
	
	printf("Escreva dois numeros inteiros: ");
	scanf("%d %d", &numero_um, &numero_dois);
	
	printf("%d + %d = %d\n", numero_um, numero_dois, numero_um + numero_dois);
	printf("%d - %d = %d\n", numero_um, numero_dois, numero_um - numero_dois);
	printf("%d * %d = %d\n", numero_um, numero_dois, numero_um * numero_dois);
	printf("%d / %d = %d\n", numero_um, numero_dois, numero_um / numero_dois);
	printf("%d %% %d = %d\n", numero_um, numero_dois, numero_um % numero_dois);
	printf("(%d + %d) * %d = %d\n", numero_um, numero_dois, numero_dois, (numero_um + numero_dois) * numero_dois);
	
	return 0;
}