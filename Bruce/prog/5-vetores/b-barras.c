// Leia um inteiro n, seguido de n inteiros.
// Cada um dos n inteiros representa a altura
// de uma barra num grafico de barras. Escreva
// esse grafico de barras usando '#'.

// Exemplo
// -------
// Entrada:
// 5
// 2 3 5 1 2
//
// Saida:
//   #
//   #
//  ##
// ### #
// #####