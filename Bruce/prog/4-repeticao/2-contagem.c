#include <stdio.h>

int main()
{
	int n;
	printf("Ate quanto eu conto? ");
	scanf("%d", &n);
	
	int c=1;
	while(c != n)
	{
		printf("%d, ", c);
		c = c+1; // Ou c += 1, ou c++, ou ++c.
	}
	printf("%d!\n", c);
	
	return 0;
}