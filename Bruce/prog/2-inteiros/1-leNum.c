#include <stdio.h>

int main()
{
	int numero; // Declara uma variavel inteira chamada numero.
	
	printf("Escreva um numero inteiro: ");
	scanf("%d", &numero);  // Le um numero inteiro e guarda na variavel numero.
	
	printf("Voce escreveu %d.\n", numero);
	
	return 0;
}