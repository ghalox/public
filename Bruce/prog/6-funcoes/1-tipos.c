#include <stdio.h>
#include <limits.h> // Para inteiros
#include <float.h> // Para ponto-flutuante

int main() {
	printf("Inteiros:\n");
	printf("Tipo                Bytes                Minimo  Maximo\n");
	printf("char                %5d  %20d  %d\n", sizeof(char), CHAR_MIN, CHAR_MAX);
	printf("unsigned char       %5d  %20u  %u\n", sizeof(unsigned char), 0, UCHAR_MAX);
	printf("short               %5d  %20d  %d\n", sizeof(short), SHRT_MIN, SHRT_MAX);
	printf("unsigned short      %5d  %20u  %u\n", sizeof(unsigned short), 0, USHRT_MAX);
	printf("int                 %5d  %20d  %d\n", sizeof(int), INT_MIN, INT_MAX);
	printf("unsigned int        %5d  %20u  %u\n", sizeof(unsigned int), 0, UINT_MAX);
	printf("long                %5d  %20ld  %ld\n", sizeof(long), LONG_MIN, LONG_MAX);
	printf("unsigned long       %5d  %20lu  %lu\n", sizeof(unsigned long), 0L, ULONG_MAX);
	printf("long long           %5d  %20lld  %lld\n", sizeof(long long), LLONG_MIN, LLONG_MAX);
	printf("unsigned long long  %5d  %20llu  %llu\n", sizeof(unsigned long long), 0LL, ULLONG_MAX);
		
	printf("\nINT_MAX = %d\n", INT_MAX);
	printf("INT_MAX + 1 = %d\n", INT_MAX+1);
	printf("UINT_MAX = %u\n", UINT_MAX);
	printf("UINT_MAX + 1 = %u\n", UINT_MAX+1);
	
	printf("\nINT_MAX+1, unsigned: %u\n", INT_MAX+1);
	printf("UINT_MAX, signed: %d\n", UINT_MAX);
	
	printf("\nPonto-flutuante:\n");
	printf("Tipo    Bytes  Precisao        Epsilon          Menor  Maior\n");
	printf("float   %5d  %8d  %e  %e  %e\n", sizeof(float), FLT_DIG, FLT_EPSILON, FLT_MIN, FLT_MAX);
	printf("double  %5d  %8d  %e  %e  %e\n", sizeof(double), DBL_DIG,  DBL_EPSILON, DBL_MIN, DBL_MAX);
	
	printf("\nMaior float, por extenso:  %f\n", FLT_MAX);
	printf("Maior double, por extenso: %lf\n", DBL_MAX);
	
	return 0;
}