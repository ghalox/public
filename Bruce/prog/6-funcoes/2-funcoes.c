#include <stdio.h>

int numero_aleatorio(){
	printf("Voce chamou a funcao numero_aleatorio.\n");
	return 6; // Foi o que saiu no dado que joguei.
}

double minha_altura(){
	printf("Voce chamou a funcao minha_altura.\n");
	return 1.82; // Foi um chute.
}

void devolve_nada(){
	printf("Voce chamou a funcao que devolve nada.\n");
	return;
}

int main() {
	printf("Numero aleatorio: %d\n", numero_aleatorio());
	printf("Altura: %lf\n", minha_altura());
	devolve_nada();
	
	int a;
	a = numero_aleatorio() + 3;
	printf("a = %d\n", a);
	
	return 0;
}