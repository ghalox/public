#include <stdio.h>
int a;
int b;

void func_um(){
	a = 3; b = 3;
	int c; c = 3;
	return;
}

void func_dois(int, int, int);

int main() {
	a = 1;
	int b; b = 1;
	int c; c = 1;
	printf("a, b, c: %d, %d, %d\n", a, b, c);
	
	printf("Chamando func_um...\n");
	func_um();
	scanf("%*[^\n]"); fflush(stdin);
	printf("a, b, c: %d, %d, %d\n", a, b, c);
	
	printf("Chamando func_dois...\n");
	func_dois(a, b, c);
	scanf("%*[^\n]"); fflush(stdin);
	printf("a, b, c: %d, %d, %d\n", a, b, c);
	
	return 0;
}

void func_dois(int a, int b, int c){
	printf("Entrei na func_dois.\n");
	printf("a, b, c: %d, %d, %d\n", a, b, c);
	printf("Alterando valores...\n");
	a = 5; b = 5; c = 5;
	printf("a, b, c: %d, %d, %d\n", a, b, c);
	printf("Sai da func_dois.\n");
	return;
}