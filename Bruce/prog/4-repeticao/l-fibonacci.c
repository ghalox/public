// A sequencia de Fibonacci comeca com os numeros 0 e 1.
// Para obter o proximo numero da sequencia, somamos os dois
// numeros anteriores. Assim, a sequencia de Fibonacci e'
// dada por
//   0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
// Porque 0+1=1, 1+1=2, 1+2=3, 2+3=5, ...

// Leia um inteiro n e escreva o n-esimo numero de Fibonacci.