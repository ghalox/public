#include <stdio.h>

int main(){
	int n;
	
	printf("x = ");
	scanf("%d", &n);
	printf("4x^2 + 1 >= x^3?\n");
	
	if(n < 0){
		if(4*n*n + 1 >= n*n*n)
			printf("Sim. %d = 4*(%d)^2+1 >= (%d)^3 = %d.\n", 4*n*n + 1, n, n, n*n*n);
		else
			printf("Nao. %d = 4*(%d)^2+1 < (%d)^3 = %d.\n", 4*n*n + 1, n, n, n*n*n);
	}else{
		if(4*n*n + 1 >= n*n*n)
			printf("Sim. %d = 4*%d^2+1 >= %d^3 = %d.\n", 4*n*n + 1, n, n, n*n*n);
		else
			printf("Nao. %d = 4*%d^2+1 < %d^3 = %d.\n", 4*n*n + 1, n, n, n*n*n);
	}
	
	return 0;
}
