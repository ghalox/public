// Leia um inteiro n e uma lista de n inteiros.
// Ate encontrar fim-de-arquivo (EOF),
// leia dois inteiros a e b tais que 0 <= a <= b < n
// e escreva a soma do a-esimo ate o b-esimo inteiro da lista.
//
// Lembre que o primeiro elemento da lista eh o 0-esimo.