// Leia um inteiro m, entao m inteiros;
// e mais um inteiro n, e n inteiros.
// Para cada inteiro x dos m inteiros,
// escreva numa linha o produto de x com
// cada um dos n inteiros.

// Exemplo
// -------
// Entrada:
// 3 -1 2 7
// 5 1 2 3 9 10
//
// Saida:
// -1 -2 -3 -9 -10
// 2 4 6 18 20
// 7 14 21 63 70