// Escreva um programa que le a idade de uma pessoa
// e diz se ela pode votar ou nao.
// Se ela puder votar, diga se o voto eh obrigatorio
// ou facultativo.

// Assuma que se a pessoa tiver idade pra votar,
// ela ja foi alfabetizada.

// Lembre-se:
// Podemos votar a partir dos 16 anos e o voto eh
// obrigatorio para todos os alfabetizados a partir
// dos 18 anos. Dos 70 anos em diante, o voto eh
// facultativo de novo.