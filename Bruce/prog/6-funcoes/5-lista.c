#include <stdio.h>
#include <stdlib.h>

int main(){
	printf("Quantos inteiros voce vai escrever? ");
	int n; scanf("%d", &n);
	
	printf("Okay. Alocando espaco para %d inteiros...", n);
	int *lista; lista = (int*) malloc( n * sizeof(int) );
	if(lista == NULL){
		fprintf(stderr, "ERRO: nao foi possivel alocar memoria. ):\n");
		return -1;
	}
	printf(" Alocado. (:\n");
	
	printf("Escreva os %d inteiros: ", n);
	int i = 0;
	while(i < n){
		scanf("%d", &(lista[i]));
		i = i + 1;
	}
	
	printf("Os inteiros que li sao: ");
	i = 0;
	while(i < n){
		printf("%d ", lista[i]);
		i = i + 1;
	}
	printf("\n");
	
	return 0;
}