Alguns comandos do Prompt de Comando
------------------------------------

dir
Lista todos os arquivos e subpastas da pasta atual.

cd <subpasta>
Entra em <subpasta>.

cd ..
Vai para pasta-pai da pasta atual.

type <arquivo>
Imprime conteúdo de <arquivo> na tela.

mkdir <nome>
Cria uma subpasta chamada <nome>.

copy <arq1> <arq2>
Copia <arq1> para <arq2>.

move <arquivo> <nome>
move <pasta> <nome>
Renomeia <arquivo> ou <pasta> como <nome>.

move <arquivo> <pasta>
Move <arquivo> para <pasta>.

del <arquivo>
Remove <arquivo> da pasta atual.

rmdir <nome>
Remove a subpasta <nome>.

gcc <código-fonte> -o <executável>
Compila o arquivo em C <código-fonte> e salva resultado como <executável>.
