// Escreva um programa que le um inteiro n.
// Se n for par, escreva n.
// Se n for impar, escreva o proximo numero par.

// Casos-teste
// Entrada   Saida
// 0         0
// 3         4
// 57        58
// -3        -2
// -2        -2