#include <stdio.h>

int main()
{
	printf("Um numero, por favor: ");
	int n; scanf("%d", &n);
	
	if( (n > 0) && (n % 2 == 1) )
		printf("%d e' impar e positivo.\n", n);
	
	if( (n > 0) || (n % 2) )
		printf("%d e' impar ou positivo.\n", n);
	
	if( !(n > 0) )
		printf("%d nao e' positivo.\n", n);
	
	return 0;
}
