#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]){
	// Mensagem de erro.
	if(argc != 4){
		fprintf(stderr, "Sintaxe: %s <quantos> <minimo> <maximo>\n", argv[0]);
		fprintf(stderr, "Este programa escreve <quantos>, pula uma linha, e entao escreve <quantos> numeros aleatorios no intervalo [<minimo>, <maximo>[.\n");
		return -1;
	}
	
	// Inicializa gerador de numeros aleatorios.
	srand(time(NULL)); rand();
	
	// Guarda parametros da linha de comando.
	int n = atoi(argv[1]), min = atoi(argv[2]), max = atoi(argv[3]);
	
	// Imprime n.
	printf("%d\n", n);
	// Imprime os n numeros aleatorios.
	int i = 0;
	while(i < n){
		printf("%d ", ((max-min) * rand() / RAND_MAX) + min);
		i = i + 1;
	}
	printf("\n");
	
	return 0;
}