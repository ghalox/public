#include <stdio.h>

int main()
{
	int n;
	
	printf("Digite um numero: ");
	scanf("%d", &n);
	
	if(n > 0)
	{
		printf("%d e' positivo.\n", n);
	}
	else
	{
		if(n == 0)
		{
			printf("%d e' zero.\n", n);
		}
		else
		{
			printf("%d e' negativo.\n", n);
		}
	}
	
	return 0;
}
